import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Network } from '@ionic-native/network';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { FCM } from '@ionic-native/fcm';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { CalendarModule } from "ion2-calendar";
import { MyApp } from './app.component';

import { UserProvider } from '../provider/userProvider';
import { SpinnerProvider } from '../provider/spinnerProvider';
import { AlertProvider } from '../provider/alertProvider';

import { Api } from '../services/api';
import { CordovaService } from '../services/cordovaService';
import { DBManager } from '../services/dbManager';
import { CartManager } from '../services/cartManager';

import { OrderListDao } from '../services/dao/orderListDao';
import { MessagesDao } from '../services/dao/messagesDao';

import { FilterPipe } from '../pipe/filterPipe';
import { MomentPipe } from '../pipe/momentPipe';

import { TabsPage } from '../pages/tabs/tabs';
import { HeaderPage } from '../pages/header/header';
import { PortalPage } from '../pages/portal/portal';
import { ActivatePage } from '../pages/activate/activate';
import { OtpPage } from '../pages/otp/otp';
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';
import { OrderFoodPage } from '../pages/orderFood/orderFood';
import { OrderConfirmPage } from '../pages/orderConfirm/orderConfirm';
import { OrderListPage } from '../pages/orderList/orderList';
import { MessagePage } from '../pages/message/message';
import { SettingPage } from '../pages/setting/setting';

import { FoodModalPage } from '../pages/foodModal/foodModal';
import { SpecialOrderModalPage } from '../pages/specialOrderModal/specialOrderModal';
import { SpecialFoodModalPage } from '../pages/specialFoodModal/specialFoodModal';
import { MessageModalPage } from '../pages/messageModal/messageModal';
import { DateModalPage } from '../pages/dateModal/dateModal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HeaderPage,
    PortalPage,
    ActivatePage,
    OtpPage,
    LoginPage,
    AccountPage,
    OrderFoodPage,
    OrderConfirmPage,
    OrderListPage,
    MessagePage,
    SettingPage,

    FoodModalPage,
    SpecialOrderModalPage,
    SpecialFoodModalPage,
    MessageModalPage,
    DateModalPage,

    FilterPipe,
    MomentPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    CalendarModule,
    IonicModule.forRoot(MyApp, {
      dayShortNames: ['日', '一', '二', '三', '四', '五', '六' ]
   })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    PortalPage,
    ActivatePage,
    OtpPage,
    LoginPage,
    AccountPage,
    OrderFoodPage,
    OrderConfirmPage,
    OrderListPage,
    MessagePage,
    SettingPage,

    FoodModalPage,
    SpecialOrderModalPage,
    SpecialFoodModalPage,
    MessageModalPage,
    DateModalPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    AppVersion,
    Device,
    UniqueDeviceID,
    FCM,
    UserProvider,
    SpinnerProvider,
    AlertProvider,
    Api,
    CordovaService,
    OrderListDao,
    MessagesDao,
    DBManager,
    CartManager,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
