import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { PortalPage } from '../pages/portal/portal';

import { CordovaService } from '../services/cordovaService';
import { DBManager } from '../services/dbManager';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public cordovaService: CordovaService,
    public dbManager: DBManager) {
    platform.ready().then(() => {
      cordovaService.init()
        .then(() => this.dbManager.init())
        .then(() => cordovaService.loadDeviceInfo().then(() => {
          cordovaService.fcmConfig();
        }))
        .then(() => {
          splashScreen.hide();
          this.rootPage = PortalPage;
          // 重開APP都要重新登入
          // if (localStorage.userId) {
          //   this.rootPage = TabsPage;
          // } else {
          //   this.rootPage = PortalPage;
          // }
        })
    });
  }
}
