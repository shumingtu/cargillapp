export class Constant {
    // public static CARGILL_API = 'https://www.ifeed.com.tw/CargillApi/api/';
    // public static CARGILL_API = 'http://10.1.3.253/CargillApi/api/';
    public static CARGILL_API = 'https://iis.apps.ocp.webcomm.com.tw/CargillApi/api/';
    // public static CARGILL_API = 'http://localhost:21514/api/';

    public static CARGILL_SLIDE_URL_PREFIX = 'https://www.ifeed.com.tw:8443/api/';

    //public static APP_DOWNLOAD_URL = 'https://www.ifeed.com.tw/CargillApp/download.html';
    public static APP_DOWNLOAD_URL = 'https://iis.apps.ocp.webcomm.com.tw/CargillApp/download.html';

    public static CS_PHONE = '0800-025-688';

    public static APP_VERSION = '20200422';
}