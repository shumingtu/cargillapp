import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertProvider {
    alert: any;

    constructor(public alertCtrl: AlertController) {
    }

    create(config) {
        if (this.alert) {
            console.log("111111");
            return;
        }
        console.log(config);
        this.alert = {};
        this.alert = this.alertCtrl.create(config);
        this.alert.present();

        this.alert.onDidDismiss(() => {
            this.clear();
        });

        return this.alert;
    }

    clear = () => {
        delete this.alert;
    }
}