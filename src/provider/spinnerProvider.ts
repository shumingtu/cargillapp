import { Injectable } from '@angular/core';

import { LoadingController } from 'ionic-angular';

@Injectable()
export class SpinnerProvider {

    constructor(public loadingCtrl: LoadingController) {
        
    }

    spinner: any;

    show() {
        console.log("載入中");
        if(this.spinner) return;
        
        this.spinner = this.loadingCtrl.create({
            content: "載入中"
        });

        this.spinner.present();
    }

    hide() {
        if(!this.spinner) return;
        
        this.spinner.dismiss();
        delete this.spinner;
    }
    
    isShow(){
        return (this.spinner) ? true: false;
    }
}