import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Api } from '../services/api';
import { Constant } from '../app/constant';
import { AccountPage } from '../pages/account/account';
import { PortalPage } from '../pages/portal/portal';
import { DBManager } from '../services/dbManager';

import * as _ from "lodash";

@Injectable()
export class UserProvider {
    subCustomers = [];
    oldRecord = {};
    newRecord = {};
    
    constructor(private api: Api, private dbManager: DBManager, private events: Events) { }

    setDeviceInfo() {
        var para = {
            deviceId: localStorage.deviceId,
            deviceType: localStorage.deviceType,
            deviceModel: localStorage.deviceModel,
            deviceResolution: localStorage.deviceResolution,
            deviceVersion: localStorage.deviceVersion,
            deviceManufacturer: localStorage.deviceManufacturer,
            deviceToken: localStorage.deviceToken,
            userId: localStorage.userId ? localStorage.userId : '0'
        };

        this.api.setDeviceInfo(para, (data) => {
        }, () => {

        });
    }

    getSubCustomers () {
        return this.subCustomers;
    }

    setSubCustomers (subCustomers) {
        this.subCustomers = subCustomers;
    }

    reSelectSbuCustomer(appCtrl){
        let appUser = JSON.parse(localStorage.getItem('appUser'));
        if(appUser.type == "MC" || appUser.type == "S"){  // 主帳號 業務
            localStorage.removeItem('account');
            localStorage.reSelectSbuCustomer = 1;
            appCtrl.getRootNav().setRoot(AccountPage);
        } else {
            localStorage.removeItem('account');
            localStorage.removeItem('appUser');
            localStorage.removeItem('token');
            appCtrl.getRootNav().setRoot(PortalPage);
        }
    }

    getMessage(){
        return new Promise((resolve, reject) => {
            let appUser = JSON.parse(localStorage.getItem('appUser'));
            let para = {
                userId: appUser.userId,
                userType: appUser.type,
              }
            this.api.getMessages(para, (data) => {
                console.log(data);
                //this.messages = data.messages;
                this.dbManager.messages.addBulkWithPromise(data.messages);
          
                //處理已讀狀態
                let tempStr = localStorage.getItem('readMessages');
                this.oldRecord = tempStr ? JSON.parse(tempStr) : {};
                this.newRecord = {};
                this.resetRecord(data.messages);
    
                let result = {
                    messages: data.messages,
                    newRecord: this.newRecord
                };
                resolve(result);
              }, () => {
                resolve();
              });
        });
    }
    getUnReadCount(){
        console.log(this.newRecord);
        let unReadCount = 0
        for(let key in this.newRecord){
            if(this.newRecord[key] == 0){
                unReadCount++;
            }
        }

        this.events.publish('tab:refreshBadge', unReadCount);

        return unReadCount;
    }
    async setRecord() {
        await _.forOwn(this.oldRecord, (value, key) => {
          if (this.newRecord.hasOwnProperty(key)) {
            if (value == 1) {
              this.newRecord[key] = 1;
            }
          }
        })
      }
    async resetRecord(messages) {
        let refreshRecord = await this.refreshRecord(messages);
        let setRecord = await this.setRecord();
        console.log(this.newRecord);
        localStorage.setItem('readMessages', JSON.stringify(this.newRecord));
        this.getUnReadCount();
      }
    
      async refreshRecord(messages) {
        await messages.forEach( message => {
          this.newRecord[message.messageId] = 0;
        })
      }
}