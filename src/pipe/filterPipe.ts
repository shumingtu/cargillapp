import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})

export class FilterPipe implements PipeTransform {
    transform(list: any[], searchText: string): any {
        if (searchText == null) {
            return list;
        }

        return list.filter((item) => {
            if (item.id.indexOf(searchText) !== -1 ||
                item.name.indexOf(searchText) !== -1 ||
                item.mcId.indexOf(searchText) !== -1 ||
                item.mcName.indexOf(searchText) !== -1 ||
                item.shipTo.indexOf(searchText) !== -1) {
                return true;
            }
        });
    }
}