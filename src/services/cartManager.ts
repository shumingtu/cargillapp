import { Injectable } from '@angular/core';

@Injectable()
export class CartManager {
    cart = {
        foods: [],
        location: null,
        date: null,
        time: null,
        note: null,
    };

    list = () => {
        return this.cart;
    }

    get = (key) => {
        return this.cart[key];
    }

    set = (key, value) => {
        this.cart[key] = value;
    }

    setOrder = (order) => {
        this.cart = order;
    }

    clear = () => {
        this.cart = {
            foods: [],
            location: null,
            date: null,
            time: null,
            note: null,
        };
    }

    remove(key){
        delete this.cart[key];
    }
}