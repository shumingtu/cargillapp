import { Injectable } from '@angular/core';
import { DexieProvider } from '../provider/dexieProvider';
import { OrderListDao } from './dao/orderListDao';
import { MessagesDao } from './dao/messagesDao';

@Injectable()
export class DBManager {
    orderList: any;
    messages: any;

    constructor (
        public orderListDao: OrderListDao,
        public messagesDao: MessagesDao) {

    }

    /*init() {
        console.log('db init start');
        return new Promise ((resolve, reject) => {
            DexieProvider.getDB().open()
            .then(() => {
                Promise.all([
                    this.orderListDao.init(),
                ]).then(() => {
                    this.orderList = this.orderListDao;

                    console.log('DB init OK');
                    resolve();
                });
            }).catch((error) => {
                console.log('DB init error: ' + error);
                reject();
            });
        });
    }*/

    async init() {
        console.log('db init start');

        try {
            await DexieProvider.getDB().open();
            await Promise.all([
                this.orderListDao.init(),
                this.messagesDao.init(),
            ]);
                
            this.orderList = this.orderListDao;
            this.messages = this.messagesDao;
            console.log('DB init OK');
        } catch (error) {
            console.log('DB init error: ' + error);
        }
    }
}