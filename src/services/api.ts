import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Constant } from '../app/constant';
import * as _ from "lodash";
import { SpinnerProvider } from '../provider/spinnerProvider';

@Injectable()
export class Api {
    orderList = [];

    constructor(
        private http: HttpClient,
        private spinnerProvider: SpinnerProvider) {
    }

    handleHttpSuccess(data, onSuccess, onError) {
        if (data.error) {
            onError(data.error);
        } else {
            onSuccess(data);
        }
    }

    handleHttpError(httpStatus, onError) {
        if (httpStatus === 500) {
            console.log('系統忙碌中');
            onError();
        } else {
            console.log('系統忙碌中，請稍候。');
            onError();
        }
    }

    getBanner(onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.get(Constant.CARGILL_API + "sample/getSlides").subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )

        /*this.handleHttpSuccess([
            { imgUrl: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif" },
            { imgUrl: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif" },
            { imgUrl: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif" },
            { imgUrl: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif" },
        ], onSuccess, onError);
        this.handleHttpSuccess({
            carousel_sec: 6,
            img: [
                {
                    file: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif",
                    link: "http://tw.yahoo.com"
                },
                {
                    file: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif",
                    link: "http://tw.yahoo.com"
                },
                {
                    file: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif",
                    link: "http://tw.yahoo.com"
                },
                {
                    file: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif",
                    link: "http://tw.yahoo.com"
                },
                {
                    file: "http://subtpg.tpg.gov.tw/web-life/taiwan/9702/image/9702-05/008.gif",
                    link: "http://tw.yahoo.com"
                },
            ]
        }, onSuccess, onError);*/
    }

    login(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "user/login", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getSubCustomers(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "user/getSubCustomers", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    setPassword(para, onSuccess, onError) {
        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "user/setPassword", para).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getOrderOptions(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "order/getOrderOptions", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    getFoodInfo(accountId, onSuccess, onError) {
        // this.spinnerProvider.show();
        // this.spinnerProvider.hide();
        this.handleHttpSuccess({
            locations: [{ id: '1', name: '臺北' }, { id: '2', name: '桃園' }],
            foods: [
                { code: '1', name: '小豬特A料', quantitys: [1, 3, 5] },
                { code: '2', name: '小豬特B料', quantitys: [2, 4, 6] },
                { code: '3', name: '小豬特C料', quantitys: [3, 6, 9] }],
            specialOrders: [
                { id: '114', name: '麵粉', maxQuantity: 3 },
                { id: '123', name: '鈣粒', maxQuantity: 5 },
                { id: '175', name: '硫酸亞鐵', maxQuantity: 7 }],
            //specialFoodQuantitys: [0.25, 0.35, 0.65]
        }, onSuccess, onError)
    }

    getOrderList(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "order/getOrders", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    //addOrderInfo(order) {
    //    this.orderList.push(order);
    //}

    addOrder(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "order/addOrder", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    editOrder(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "order/editOrder", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    deleteOrder(para, onSuccess, onError) {
        /*_.remove(this.orderList, function (orderInfo) {
            return orderInfo == order;
        });*/

        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "order/deleteOrder", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    //訊息
    getMessages(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "message/getMessages", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    deleteMessages(para, onSuccess, onError) {
        let token = localStorage.getItem('token');
        let headers = new Headers();
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            })
        };

        this.spinnerProvider.show();
        this.http.post(Constant.CARGILL_API + "message/deleteMessages", para, options).subscribe(
            data => {
                this.spinnerProvider.hide();
                this.handleHttpSuccess(data, onSuccess, onError);
                console.log(data);
            },
            error => {
                this.spinnerProvider.hide();
                this.handleHttpError(error.status, onError);
                console.log(error);
            }
        )
    }

    checkAppVersion(deviceType, version, onSuccess, onError) {
        this.http.get(Constant.CARGILL_API + "user/checkAppVersion?deviceType=" + deviceType + "&version=" + version).subscribe(
            data => {
                //this.handleHttpSuccess(data, onSuccess, onError);
                onSuccess(data);
            },
            error => {
                //console.log(error);
                this.handleHttpError(error.status, onError);
            });
    }

    setDeviceInfo(para, onSuccess, onError) {
        this.http.post(Constant.CARGILL_API + "user/setDeviceInfo", para).subscribe(
            data => {
                this.handleHttpSuccess(data, onSuccess, onError);
            },
            error => {
                console.log(error);
                this.handleHttpError(error.status, onError);
            });
    }
}