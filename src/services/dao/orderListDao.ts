import { Injectable } from '@angular/core';
import { DexieProvider } from '../../provider/dexieProvider';

import * as _ from "lodash";

@Injectable()
export class OrderListDao {
    orderList = [];
    table: any = DexieProvider.getDB().orderList;

    constructor() {
    }

    async init() {
        try {
            let counter = 0;
            let collection = await this.table.toCollection();
            let count = await collection.count();

            if (count !== 0) {
                await collection.each((item) => {
                    this.orderList.push(item);
                })
            }
            console.log('init OK');
        } catch (err) {
            console.error(err);
            throw new Error(err);
        }
    }

    async addWithPromise(item) {
        try {
            await this.table.put(item);
            this.orderList.push(item);
        } catch (err) {
            console.error(err);
            throw new Error(err);
        }
    }

    async putWithPromise(item) {
        try {
            let index = await _.findIndex(this.orderList, function (order) {
                return order.id == item.id;
            });
            console.log(index);

            if (index < 0) {
                //console.log('invalid item');
                throw new Error('invalid item: ' + index);
            }

            await this.table.put(item);
            console.log('put OK');
            this.orderList[index] = item;
        } catch (err) {
            console.log(err);
            throw new Error(err);
        }
    }

    async putBulkWithPromise(items) {
        try {
            await this.table.bulkPut(items);
            this.orderList = [];
            this.orderList = _.cloneDeep(items);
            console.log('bulk put ok');
        } catch (err) {
            console.log(err);
            throw new Error(err);
        }
    }

    async getById(id) {
        let data = await this.table.get(id);

        return data;
    }

    list() {
        console.log(this.orderList);
        return this.orderList;
    }

    async deleteBulkWithPromise(ids) {
        let data = await this.table.bulkDelete(ids);

        return data;
    }

    async clear() {
        await this.table.clear();
    }

    async count() {
        let count = await this.table.count();

        return count;
    }
}