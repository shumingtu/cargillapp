import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { Api } from '../../services/api';
import { AlertProvider } from '../../provider/alertProvider';
import { AccountPage } from '../account/account';
import { TabsPage } from '../tabs/tabs';
import { UserProvider } from '../../provider/userProvider';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  isSaveAccount = false;
  errorMsg = "";
  loginData = { userId: "", 
                password: ""};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertProvider: AlertProvider,
    public userProvider: UserProvider,
    public api: Api) {
      // this.loginData.userId = "211";
      // this.loginData.password = "8888";
  }

  public editForm: FormGroup = new FormGroup({
    'userId': new FormControl('', Validators.required),
    'password': new FormControl('', Validators.required),
    'isSaveAccount': new FormControl(''),
  });

  ionViewWillEnter() {
    if(localStorage.saveAccount){
      this.isSaveAccount = true;
      this.loginData.userId = localStorage.saveAccount;
    }
  }

  setSaveAccount(){
    console.log(this.isSaveAccount);
    if(this.isSaveAccount){
      localStorage.saveAccount = this.loginData.userId;
    } else {
      localStorage.removeItem("saveAccount");
    }
  }

  login() {
    console.log(this.loginData);

    if (this.loginData.userId.trim() == "" || this.loginData.password.trim() == "") {
      let alert = this.alertProvider.create({
        title: "提醒",
        message: "尚未輸入帳號/密碼",
        buttons: [{
          text: "確認",
        }]
      });
      //alert.present();
      return;
    }

    console.log(this.editForm.value);

    // 登入app帳號
    this.api.login(this.editForm.value, (account) => {
      console.log(account.type);
      localStorage.setItem('userId', this.editForm.value.userId);
      this.userProvider.setDeviceInfo();
      //儲存token
      localStorage.setItem('token', account.token);

      // 取得操作客戶資料
      this.api.getSubCustomers({ userId: this.editForm.value.userId, type: account.type }, (data) => {
        console.log(data.subCustomers);
        this.userProvider.setSubCustomers(data.subCustomers);

        if (account.type == "MC" || account.type == "S") {
          localStorage.setItem('account', JSON.stringify(data.subCustomers));
          localStorage.setItem('appUser', JSON.stringify({
            userId: this.editForm.value.userId,
            name: account.name,
            type: account.type
          })
          );
          this.setSaveAccount();
          this.navCtrl.push(AccountPage, { accounts: data.subCustomers }, { animate: false });
        } else if (account.type == "SC") {
          localStorage.setItem('account', JSON.stringify(data.subCustomers[0]));
          localStorage.setItem('appUser', JSON.stringify({
            userId: this.editForm.value.userId,
            name: account.name,
            type: account.type
          })
          );
          this.setSaveAccount();
          this.navCtrl.push(TabsPage, { animate: false });
        } else {
          this.errorMsg = "帳號錯誤";
        }
      }, () => {
        this.errorMsg = "子帳號錯誤";
      })
    }, (err) => {
      this.errorMsg = err;
    })
  }
}
