import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, App, Content } from 'ionic-angular';

import { Api } from '../../services/api';
import { DBManager } from '../../services/dbManager';
import { UserProvider } from '../../provider/userProvider';
import { AlertProvider } from '../../provider/alertProvider';

import { MessageModalPage } from '../messageModal/messageModal';

import * as _ from "lodash";

@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {
  @ViewChild(Content) content: Content;

  appUser;
  tempMessages;
  //messages = [];
  messages = { create: [], complete: [], delete: [], remind: [] };
  newRecord = {};
  unReadCount = 0;
  type;

  isSelectMode = 0;
  selectedIds = [];

  cssTimer;
  cssTrick = "";

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public api: Api,
              public dbManager: DBManager,
              private userProvider: UserProvider, 
              public alertProvider: AlertProvider,
              public appCtrl: App) {
  }

  ionViewWillLoad() {
    this.appUser = JSON.parse(localStorage.getItem('appUser'));
  }

  ionViewWillEnter() {
    this.refreshData()
    this.type = "0";
    this.isSelectMode = 0;
  }

  ionViewDidEnter(){
    this.setUnReadCount();
  }

  refreshData() {
    this.userProvider.getMessage().then((result) => {
      console.log(result);
      this.newRecord = result['newRecord'];
      this.tempMessages = result['messages'];

      this.messageClassify();
    });
  }

  typeChange() {
    this.content.scrollToTop();
  }

  changeMode(value) {
    //console.log(value);

    if (value == 0) {
      this.resetSelect();
      this.isSelectMode = 0;
    } else {
      this.isSelectMode = 1;

      if (value == 2) { // 全選
        /*this.tempMessages.forEach(message => {
          message.isSelected = true;
          this.selectedIds.push(message.messageId);
        })*/

        //該頁全選
        switch (this.type) {
          case '0':
            this.messages.create.forEach(message => {
              message.isSelected = true;
              this.selectedIds.push(message.messageId);
            })
            break;
          case '1':
            this.messages.complete.forEach(message => {
              message.isSelected = true;
              this.selectedIds.push(message.messageId);
            })
            break;
          case '2':
            this.messages.delete.forEach(message => {
              message.isSelected = true;
              this.selectedIds.push(message.messageId);
            })
            break;
          case '3':
            this.messages.remind.forEach(message => {
              message.isSelected = true;
              this.selectedIds.push(message.messageId);
            })
            break;
          default:
            console.log("type error");
            break;
        }


        //console.log(this.selectedIds);
      }
    }
  }

  selectMessage(message) {
    if (this.isSelectMode) {
      this.selectDelete(message);
    } else {
      this.openMessageModal(message);
    }
  }

  resetSelect() {
    this.selectedIds.length = 0;
    this.tempMessages.forEach(message => {
      message.isSelected = false;
    })
  }

  submit() {
    console.log(this.selectedIds);
    console.log(this.newRecord);
    
    if (this.selectedIds.length == 0) {
      this.alertProvider.create({
        message: "沒有選取訊息",
        buttons: [{
          text: "確定",
        }]
      });
      return;
    }

    if (this.isSelectMode == 1) { //標記已讀
      this.selectedIds.forEach(id => {
        this.newRecord[id] = 1;
      })
      localStorage.setItem('readMessages', JSON.stringify(this.newRecord));
      this.setUnReadCount();
      
      this.resetSelect();
      this.isSelectMode = 0;
    } else { //刪除
      this.alertProvider.create({
        message: "功能尚未啟用",
        buttons: [{
          text: "確定",
        }]
      });
      return;
    }
  }

  deleteMessage(type) {
    console.log(type);
    let tempIds = [];

    switch (type) {
      case 0:
        this.messages.create.forEach(message => {
          tempIds.push(message.messageId);
        })
        break;
      case 1:
        this.messages.complete.forEach(message => {
          tempIds.push(message.messageId);
        })
        break;
      case 2:
        this.messages.delete.forEach(message => {
          tempIds.push(message.messageId);
        })
        break;
      case 3:
        this.messages.remind.forEach(message => {
          tempIds.push(message.messageId);
        })
        break;
      default:
        console.log("type error");
        break;
    }

    let param = {
      userId: this.appUser.userId,
      userType: this.appUser.type,
      messageIds: tempIds
    };

    console.log(param);

    this.api.deleteMessages(param, (data) => {
      console.log(data);
      this.alertProvider.create({
        message: "刪除訊息成功",
        enableBackdropDismiss: false,
        buttons: [{
          text: "確定",
          handler: () => {              
            this.resetSelect();
            this.isSelectMode = 0;
            tempIds.length = 0;
            this.refreshData();
          }
        }]
      });
    }, (error) => {
      console.log(error);
      this.alertProvider.create({
        message: "刪除訊息失敗<br/>" + error,
        buttons: [{
          text: "確定",
          handler: () => {
            this.resetSelect();
            this.isSelectMode = 0;
            tempIds.length = 0;
          }
        }]
      })
    });
  }

  openMessageModal(message) {
    const messageModal = this.modalCtrl.create(MessageModalPage, {
      message: message
    });
    messageModal.onDidDismiss(() => {
      this.setUnReadCount();
    });
    messageModal.present();
    this.newRecord[message.messageId] = 1;
    localStorage.setItem('readMessages', JSON.stringify(this.newRecord));
    console.log(this.newRecord);
  }

  selectDelete(message) {
    if (message.isSelected) {
      message.isSelected = false;
      _.remove(this.selectedIds, function(n) {return n == message.messageId;});
    } else {
      message.isSelected = true;
      this.selectedIds.push(message.messageId);
    }
  }

  setUnReadCount(){
    this.unReadCount = this.userProvider.getUnReadCount();
    console.log(this.unReadCount);
  }

  messageClassify () {
    return new Promise((resolve, reject) => {
      let tempStr;
      this.messages = { create: [], complete: [], delete: [], remind: []};

      this.tempMessages.forEach(message => {
        message.isSelected = false; //給初始值
        tempStr = "";
        tempStr = message.title.substring(0,3);

        if (tempStr.includes('新增') || tempStr.includes('修改')) {
          this.messages.create.push(message);
        } else if (tempStr.includes('完成')) {
          this.messages.complete.push(message);
        } else if (tempStr.includes('刪除')) {
          this.messages.delete.push(message);
        } else if (tempStr.includes('提醒')) {
          this.messages.remind.push(message);
        }
      });

      this.cssTimer = setTimeout(() => {
        if (this.cssTrick == "") {
          this.cssTrick = "　";
        } else {
          this.cssTrick = "";
        }
      }, 1000);
      
      resolve();
    });
  }
}
