import { Component, ViewChild } from '@angular/core';
import { Events, Tabs } from 'ionic-angular';
import { OrderFoodPage } from '../orderFood/orderFood';
import { OrderListPage } from '../orderList/orderList';
import { MessagePage } from '../message/message';
import { SettingPage } from '../setting/setting';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild(Tabs) tabs: Tabs;
  OrderFoodRoot = OrderFoodPage;
  OrderListRoot = OrderListPage;
  MessageRoot = MessagePage;
  SettingRoot = SettingPage;
  badgeNum = 0;
  
  constructor(public events: Events) {
    events.subscribe('tab:refreshBadge', (badgeNum) => {
      this.badgeNum = badgeNum;
    });
  }
}
