import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';

import { Api } from '../../services/api';
import { Constant } from '../../app/constant';

import { LoginPage } from '../login/login';
import { ActivatePage } from '../activate/activate';
@Component({
  selector: 'page-portal',
  templateUrl: 'portal.html',
})
export class PortalPage {
  @ViewChild(Slides) slides: Slides;

  slidePageTime;
  slideDatas = [];
  slideURLPrefix = Constant.CARGILL_SLIDE_URL_PREFIX;
  phone = Constant.CS_PHONE;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: Api) {
  }

  ionViewWillEnter() {
    this.api.getBanner((slideDatas) => {
      this.slidePageTime = slideDatas.time;
      this.slideDatas = slideDatas.slides;

      console.log(this.slides);

      console.log(this.slidePageTime);
      console.log(this.slideDatas);
    }, () => { })
  }

  ionViewDidEnter() {
    if (this.slidePageTime > 0) {
      this.slides.autoplay = this.slidePageTime * 1000;
      console.log("set slide time");
    }
  }

  goSlideLink() {
    let index = this.slideIndexProc();
    let slide = this.slideDatas[index];

    console.log(slide.link);
    if(slide.link){
      window.open(slide.link, '_system',
      'enableViewportScale=yes,location=no,toolbar=yes,hardwareback=yes,clearcache=yes,clearsessioncache=yes,enableHardwareBack=no');
    }
  }
  slideIndexProc = () => {
      return this.slides._slides[this.slides.clickedIndex].getAttribute('data-swiper-slide-index');
    }

  goLogin() {
    this.navCtrl.push(LoginPage, { animate: false });
  }

  goActivate() {
    this.navCtrl.push(ActivatePage, { animate: false });
  }

  ionSlideAutoplayStop() {
    this.slides.startAutoplay();
  }

  call() {
    let element: HTMLElement = document.getElementById('portalPhone') as HTMLElement;
    element.click();
  }
}
