import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Api } from '../../services/api';
import { AlertProvider } from '../../provider/alertProvider';
import { AccountPage } from '../account/account';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  @ViewChild('textBox1') textBox1: ElementRef ;
  @ViewChild('textBox2') textBox2: ElementRef ;
  @ViewChild('textBox3') textBox3: ElementRef ;
  @ViewChild('textBox4') textBox4: ElementRef ;

  errorMsg = "";
  activateData;
  otpString: string;
  otp1: string;
  otp2: string;
  otp3: string;
  otp4: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: Api,
    public alertProvider: AlertProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  ionViewWillEnter() {
    this.activateData = this.navParams.get('activateData');
    console.log(this.activateData);
  }

  autoTab(index) {
    if (index == 1) {
      let length = this.textBox1.nativeElement.value.length;
      if (length == 1) {
        this.textBox2.nativeElement.focus();
      }
    } else if (index == 2) {
      let length = this.textBox2.nativeElement.value.length;
      if (length == 0) {
        this.textBox1.nativeElement.focus();
      }
      if (length == 1) {
        this.textBox3.nativeElement.focus();
      }
    } else if (index == 3) {
      let length = this.textBox3.nativeElement.value.length;
      if (length == 0) {
        this.textBox2.nativeElement.focus();
      }
      if (length == 1) {
        this.textBox4.nativeElement.focus();
      }
    } else if (index == 4) {
      let length = this.textBox4.nativeElement.value.length;
      if (length == 0) {
        this.textBox3.nativeElement.focus();
      }
    } else {
      console.log("invalid index: " + index);
    }

  }

  enableAccount() {
    let otpString = this.otp1 + this.otp2 + this.otp3 + this.otp4;
    console.log(otpString);

    let regixOTP = /^[0-9]{4}$/;

    if ( otpString.trim() == "" || otpString.length != 4 ) {
      this.errorMsg = "驗證碼長度錯誤";
      return;
    }

    if ( !regixOTP.test(otpString) ) {
      this.errorMsg = "驗證碼格式錯誤";
      return;
    }

    this.activateData.otp = otpString;
    console.log(this.activateData);

    // 設定密碼 改帳號狀態
    this.api.setPassword(this.activateData, () => {
      console.log("set password ok");

      // 登入app帳號
      this.api.login(this.activateData, (account) => {
        console.log(account.type);

        //儲存token
        localStorage.setItem('token', account.token);

        // 取得操作客戶資料
        this.api.getSubCustomers({ userId: this.activateData.userId, type: account.type}, (data) => {
          console.log(data.subCustomers);

          if (account.type == "MC" || account.type == "S") {
            localStorage.setItem('account', JSON.stringify(data.subCustomers));
            localStorage.setItem('appUser', JSON.stringify({
                userId: this.activateData.userId,
                name: account.name,
                type: account.type
              })
            );
            this.navCtrl.push(AccountPage, { accounts: data.subCustomers }, { animate: false });
          } else if (account.type == "SC") {
            localStorage.setItem('account', JSON.stringify(data.subCustomers[0]));
            localStorage.setItem('appUser', JSON.stringify({
                userId: this.activateData.userId,
                name: account.name,
                type: account.type
              })
            );
            this.navCtrl.push(TabsPage, { animate: false });
          } else {
            this.errorMsg = "帳號錯誤";
          }
        }, () => {
          this.errorMsg = "子帳號錯誤";
        })
      }, () => {
        this.errorMsg = "帳號/密碼輸入錯誤！";
      })
    }, (error) => {
      this.errorMsg = error;
    })
  }
}
