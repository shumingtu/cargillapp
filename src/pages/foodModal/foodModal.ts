import { Component, Renderer } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-foodModal',
  templateUrl: 'foodModal.html',
})
export class FoodModalPage {
  foodOptions;
  title;
  foodId;
  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public renderer: Renderer) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'foodModal', true);
  }
  ionViewWillLoad() {
    this.foodOptions = this.navParams.get('foodOptions');
    console.log(this.foodOptions);
    console.log(this.navParams.get('food'));
    this.foodId = this.navParams.get('food').code != 0 ? this.navParams.get('food').code : this.foodOptions[0].foodId;
    this.title = this.navParams.get('title');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.foodId);
  }
}
