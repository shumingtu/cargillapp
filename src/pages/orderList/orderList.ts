import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

import { Api } from '../../services/api';
import { AlertProvider } from '../../provider/alertProvider';
import { DBManager } from '../../services/dbManager';
import * as _ from "lodash";
import { OrderFoodPage } from '../orderFood/orderFood';
import { CartManager } from '../../services/cartManager';
import { UserProvider } from '../../provider/userProvider';

@Component({
  selector: 'page-order-list',
  templateUrl: 'orderList.html',
})
export class OrderListPage {
  account;
  appUser;
  tempOrders = [];
  orders = [];
  timeFilter;
  sortType;
  autoOn;
  //autoMessage;
  myTimer;
  
  countDownTime;
  countDownTimer;
  countDownValue;

  cssTimer;
  cssTrick = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: Api,
    public alertProvider: AlertProvider,
    public dbManager: DBManager,
    public cartManager: CartManager,
    private userProvider: UserProvider, 
    public appCtrl: App) {
  }

  ionViewWillLoad() {
    this.account = JSON.parse(localStorage.getItem('account'));
    this.appUser = JSON.parse(localStorage.getItem('appUser'));
    
    this.sortType = "1";
    this.timeFilter = "3m";
    this.autoOn = false;
    //this.autoMessage = "停止刷新";
  }

  ionViewWillEnter() {
    this.autoOn = false;
    this.countDownTime = 10;
    this.countDownValue = this.countDownTime;

    this.refreshOrders();
    console.log(this.autoOn);
    //this.setRefreshTimer();
    /*
    let para = {
      customerId: this.account.id,
    }

    // 撈DB資料
    this.api.getOrderList(para, (orderList) => {
      console.log(orderList.orders);
      
      // parse訂單結構字串
      _.forEach(orderList.orders, (order) => {
        order.details = JSON.parse(order.details);
      });
      this.tempOrders = orderList.orders;
      
      //將訂單資料清空
      this.dbManager.orderList.clear().then(() => {

        //將訂單資料存到indexedDB
        this.dbManager.orderList.putBulkWithPromise(this.tempOrders).then(() => {

          //處理成頁面用的格式
          this.orders = this.tempOrders;
          _.forEach(this.orders, (order) => {
            order.isOpen = false;
          })
          console.log(this.orders);
          console.log('save orders ok');
        }, (err) => {
          console.log('save orders failed');
        });
      });
    }, (err) => {
      console.log(err);
    });
    */
  }

  /*ionViewDidEnter() {
    console.log("enter");
      this.cssTrick = "　";
  }*/

  ionViewWillLeave() {
    console.log("leave page");
    //this.autoOn = false;
    clearInterval(this.myTimer);
    clearInterval(this.countDownTimer);
  }

  segmentChange() {
    console.log(this.timeFilter);
    this.refreshOrders();
  }

  sortSegmentChange() {
    this.sortOrder();
  }

  setAutoRefresh() {
    this.autoOn = !this.autoOn;
    //this.autoMessage = this.autoOn ? "自動刷新" : "停止刷新";

    this.setRefreshTimer();
    /*if (this.autoOn) {
      this.myTimer = setInterval(() => {
        this.refreshOrders();
      }, 1000 * 30);
    } else {
      clearInterval(this.myTimer);
    }*/
    
    console.log(this.autoOn);
  }

  setRefreshTimer() {
    /*if (this.autoOn) {
      this.myTimer = setInterval(() => {
        this.refreshOrders();
        this.countDownValue = this.countDownTime + 1;
      }, 1000 * this.countDownTime);
      this.countDownTimer = setInterval(() => {
        this.countDownValue = this.countDownValue - 1;
      }, 1000 * 1);
    } else {
      clearInterval(this.myTimer);
      clearInterval(this.countDownTimer);
      this.countDownValue = this.countDownTime;
    }*/

    //只會刷新一次
    this.myTimer = setTimeout(() => {
      this.refreshOrders();
      clearInterval(this.countDownTimer);
      this.autoOn = false;
      this.countDownValue = this.countDownTime;
    }, 1000 * this.countDownTime);
    this.countDownTimer = setInterval(() => {
      this.countDownValue = this.countDownValue - 1;
    }, 1000 * 1);
  }

  refreshOrders() {
    let para = {
      customerId: this.account.id,
      timeFilter: this.timeFilter,
    }

    // 撈DB資料
    this.api.getOrderList(para, (orderList) => {
      console.log(orderList.orders);
      
      // parse訂單結構字串
      _.forEach(orderList.orders, (order) => {
        order.details = JSON.parse(order.details);
      });
      this.tempOrders = orderList.orders;
      
      //將訂單資料清空
      this.dbManager.orderList.clear().then(() => {

        //將訂單資料存到indexedDB
        this.dbManager.orderList.putBulkWithPromise(this.tempOrders).then(() => {

          //處理成頁面用的格式
          this.orders = this.tempOrders;
          _.forEach(this.orders, (order) => {
            order.isOpen = false;
          });
          this.sortOrder();

          console.log(this.orders);
          console.log('save orders ok');
          this.cssTimer = setTimeout(() => {
            if (this.cssTrick == "") {
              this.cssTrick = "　";
            } else {
              this.cssTrick = "";
            }
          }, 1000);
        }, (err) => {
          console.log('save orders failed');
        });
      });
    }, (err) => {
      console.log(err);
    });
  }

  sortOrder(){
    if(this.sortType == "1"){
      this.orders = _.orderBy(this.orders, ['create_time'], ['desc']);
    } else {
      this.orders = _.orderBy(this.orders, ['delivery_date'], ['desc']);
    }
  }

  openOrder(order) {
    order.isOpen = !order.isOpen;
  }

  editOrder(order) {
    console.log("edit order");
    order.isEdit = true;
    this.cartManager.setOrder(order);
    this.navCtrl.parent.select(0);
  }

  copyOrder(order) {
    console.log("copy order");
    order.isCopy = true;
    this.cartManager.setOrder(order);
    this.navCtrl.parent.select(0);
  }

  deleteOrder(order) {
    let alert = this.alertProvider.create({
      message: "確定要取消訂單?",
      enableBackdropDismiss: false,
      buttons: [
        {
          text: "取消",
          handler: () => {
            console.log("cancel delete");
          }
        },
        {
          text: "確定",
          handler: () => {
            this.cartManager.set("isOrdering", true);
            console.log("call delete");
            alert.dismiss().then(() => {
              let para = {
                orderNo: order.order_no,
                customerId: this.account.id,
                orderPerson: this.appUser.name + "(" + this.appUser.userId + ")"
              };
              this.api.deleteOrder(para, () => {
                console.log("call delete success");
                this.alertProvider.create({
                  message: "取消訂單成功",
                  enableBackdropDismiss: false,
                  buttons: [{
                    text: "確定",
                    handler: () => {
                      this.refreshOrders();
                      this.userProvider.getMessage().then((result) => { });
                      this.cartManager.remove("isOrdering");
                    }
                  }]
                })
              },(err) =>{
                console.log("call delete failed");
                this.alertProvider.create({
                  message: "取消訂單失敗",
                  buttons: [{
                    text: "確定",
                  }]
                })
                console.log(err);
                this.cartManager.remove("isOrdering");
              });
            });
            return false; //讓前一個alert結束
          }
        }
    ]
    })
  }
  
  back(){
    this.userProvider.reSelectSbuCustomer(this.appCtrl);
  }
}
