import { Component } from '@angular/core';
import { NavController, ModalController, NavParams, App } from 'ionic-angular';

import { Api } from '../../services/api';
import { DBManager } from '../../services/dbManager';
import { CartManager } from '../../services/cartManager';
import { AlertProvider } from '../../provider/alertProvider';

import { FoodModalPage } from '../foodModal/foodModal';
import { SpecialOrderModalPage } from '../specialOrderModal/specialOrderModal';
import { DateModalPage } from '../dateModal/dateModal';
import { OrderListPage } from '../orderList/orderList';
import { OrderConfirmPage } from '../orderConfirm/orderConfirm';
import { UserProvider } from '../../provider/userProvider';

import * as _ from "lodash";
import * as moment from "moment";

@Component({
  selector: 'page-order-food',
  templateUrl: 'orderFood.html',
})
export class OrderFoodPage {
  account;
  orderOptions;
  info;
  location;
  dateParam;
  weekDay;
  shipDate;
  shipTime;
  foodCart;
  packages = [{name: '散裝', value: '散裝/X'}, {name:'包裝', value: '30kg/3'}];
  specs = [{name: '粒料(PL)', value: 'PL'}, {name:'粉料(ML)', value: 'ML'}, {name:'碎粒(CR)', value: 'CR'}];
  //quantity;
  //quantitys;
  note;
  isEdit = false;
  
  weekdayStr = ["日", "一", "二", "三", "四", "五", "六"];
  defaultCanDeliveredyHours = ["06:00", "07:00", "08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00"];
  todayCanDeliveredyHours;
  canDeliveredTime = {
    canDeliveredDate : moment().format('YYYY-MM-DD'),
    canDeliveredyHours : this.defaultCanDeliveredyHours
  };
  canDeliveredMaxDate = moment().add('year', 1).format('YYYY-MM-DD');

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private navParams: NavParams,
    private api: Api,
    private dbManager: DBManager,
    private cartManager: CartManager,
    public alertProvider: AlertProvider,
    private userProvider: UserProvider, 
    public appCtrl: App
  ) {
    this.initFoodCart();
  }

  ionViewWillLoad() {
    this.userProvider.getMessage().then((result) => {
      console.log(result);
    });
  }

  ionViewWillEnter() {
    //this.checkFromCopyOrEdit(); 
    //this.foodCart.foods = this.cartManager.get('foods');
    //this.foodCart.location = this.cartManager.get('location');
    //this.foodCart.date = this.cartManager.get('date');
    //this.foodCart.time = this.cartManager.get('time');
    //this.foodCart.note = this.cartManager.get('note');

    //console.log(this.foodCart);

    this.account = JSON.parse(localStorage.getItem('account'));

    let para = {
      customerId: this.account.id,
    }

    if (this.account) {
      this.api.getOrderOptions(para, (options) => {
        this.info = options.orderOptions;
        console.log(this.info);
        console.log(options.canDeliveredTime);
        this.todayCanDeliveredyHours = options.canDeliveredTime.canDeliveredyHours;
        this.canDeliveredTime = options.canDeliveredTime;

        this.checkFromCopyOrEdit();

        if (!this.foodCart.date) {
          this.weekDay = "";
        }
      }, () => { });
    }

    /*if (this.account) {
      this.api.getFoodInfo(this.account.id, (orderInfo) => {
        this.info = orderInfo;
        console.log(this.info);
      }, () => { });
    }*/
  }

  openCalendarModal() {
    if (this.foodCart.date) {
      this.dateParam = moment(this.foodCart.date, "YYYY-MM-DD").toDate();
    } else {
      this.dateParam = null;
    }

    const dateModal = this.modalCtrl.create(DateModalPage, {
      date: this.dateParam,
    }, { showBackdrop: false, enableBackdropDismiss: false });
    dateModal.present();
    dateModal.onDidDismiss((date) => {
      console.log(date);

      if(!date){
        return;
      }
      
      this.foodCart.date = date;
      this.weekDay = this.foodCart.date + " " + this.weekdayStr[moment(this.foodCart.date, "YYYY-MM-DD").weekday()];
      console.log(this.weekDay);
      this.onTimeChange();
    });

  }

  onTimeChange(){
    console.log(this.foodCart.date);
    this.foodCart.time = "";
    if(this.foodCart.date != moment().format('YYYY-MM-DD')){
      this.canDeliveredTime.canDeliveredyHours = this.defaultCanDeliveredyHours;
    } else {
      this.canDeliveredTime.canDeliveredyHours = this.todayCanDeliveredyHours;
    }
  }
  
  checkFromCopyOrEdit(){
    let order = this.cartManager.list();
    console.log(order);
    if(order["isCopy"] || order["isEdit"]){
      //檢查location下拉選單中有沒有， 名稱可能已經改了
      this.info.shipToOptions.forEach((shipToOption) => {
        if(shipToOption.address == order["ship_to"]){
          this.foodCart.location = order["ship_to"];
        }
      });

      if (order["isEdit"]) {
        //保留時間
        this.foodCart.date = moment(order["delivery_date"]).format('YYYY-MM-DD');
        this.weekDay = this.foodCart.date + " " + this.weekdayStr[moment(this.foodCart.date, "YYYY-MM-DD").weekday()];
        this.foodCart.time = moment(order["delivery_date"]).format('HH:mm');
        //更新時間選項
        if(this.foodCart.date != moment().format('YYYY-MM-DD')){
          this.canDeliveredTime.canDeliveredyHours = this.defaultCanDeliveredyHours;
        } else {
          this.canDeliveredTime.canDeliveredyHours = this.todayCanDeliveredyHours;
        }
      } else {
        this.foodCart.date = "";
        this.weekDay = "";
        this.foodCart.time = "";
      }
  
      order["details"].forEach((food) => {
        if(food.unit != "散裝/X"){
          food.unit = "30kg/3";
        }
        console.log(food.unit);

        if(!food.detailSP){
          /*food.detailSP = {
            id: "",
            value: "特配單",
            createDate: moment().format('YYYY-MM-DD HH:mm'),
            symptom: [],
            deliveryDate: "",
            productSpec: "",
            productLineNote: "",
            productNote: "",
            noteA: "",
            noteB: "",
            noteC: "",
            drugCompanyList: [],
            drugSelfList: [],
            totalPrice: 0,
            codeWeight: 0.0,
            spPrice: 0.0,
            modifyLogs: [],
          };*/
        } else {
          //重設日期
          if(order["isCopy"] && food.detailSP['createDate']) {
            food.detailSP.createDate = moment().format('YYYY-MM-DD HH:mm');
          }
        }
      });

      this.foodCart.foods = order["details"];
      this.foodCart.foods.forEach((food) => {
        food.isOpen = false;
      });
      this.foodCart.note = order["desc_app"];
    }
    
    if(order["isEdit"]){
      this.isEdit = true;
      this.foodCart["orderNo"] = order["order_no"];
    }

    if(order["modify_time"]){
      this.foodCart["modify_time"] = order["modify_time"];
    }
        
    console.log(this.foodCart);
    this.cartManager.clear();
  }

  addFood() {
    this.foodCart.foods.push({
      code: null,
      name: null,
      unit: null,
      count: null,
      isOpen: false,
      spec: null,
      weight: 0.0,
      phase: "",
      price: 0.0,
    });
  }

  deleteFood(food) {
    _.remove(this.foodCart.foods, function (foodInCart) {
      return foodInCart == food;
    });
  }

  openFood(food) {
    food.isOpen = !food.isOpen;
    let openFood = _.find(this.info.foodOptions, function (food) {
      return food.name == food.name;
    });
    //this.packages = openFood.packages;
    //this.quantitys = openFood.quantitys;
  }

  openFoodModal(food) {
    console.log(food);
    const foodModal = this.modalCtrl.create(FoodModalPage, {
      foodOptions: this.info.foodOptions,
      food: food,
      title: '選擇飼料'
    });
    foodModal.present();
    foodModal.onDidDismiss((chooseFoodId) => {
      console.log(chooseFoodId);
      if (chooseFoodId && chooseFoodId != food.code) {
        food.code = chooseFoodId;
        let chooseFood = _.find(this.info.foodOptions, function (option) {
          return option.foodId == chooseFoodId;
        });
        console.log(chooseFood);
        food.name = chooseFood.foodName;
        food.unit = null;
        food.count = null;
        food.spec = null;
      }
    })
  }

  openSpecailOrderModal(food) {
    console.log(food);
    console.log(this.info.drugOptions);
    const specialOrderModal = this.modalCtrl.create(SpecialOrderModalPage, {
      drugOptions: this.info.drugOptions,
      //specialOrderQuantitys: this.info.specialOrderQuantitys
    }, { showBackdrop: false, enableBackdropDismiss: false });
    specialOrderModal.present();
    specialOrderModal.onDidDismiss((drug) => {
      console.log(drug);

      if(!drug){
        return;
      }

      if(!drug.name || !drug.count1kg || !this.checkNumber(drug.count1kg)){
        this.showErrorMsg();
        return;
      }
      
      if (!food.detailSP) {
        food.detailSP = {
          id: "",
          value: "特配單",
          createDate: moment().format('YYYY-MM-DD HH:mm'),
          symptom: [],
          deliveryDate: "",
          productSpec: "",
          productLineNote: "",
          productNote: "",
          noteA: "",
          noteB: "",
          noteC: "",
          drugCompanyList: [],
          drugSelfList: [],
          totalPrice: 0,
          codeWeight: 0.0,
          spPrice: 0.0,
          modifyLogs: [],
        }
      }

      if (drug.type == "COMPANY") {
        food.detailSP.drugCompanyList.push(drug);
      } else if (drug.type == "SELF") {
        food.detailSP.drugSelfList.push(drug);
      }
    });

  }

  deleteSpecialOrder(food, type, order) {
    if (type == "COMPANY") {
      _.remove(food.detailSP.drugCompanyList, function (drug) {
        return drug == order;
      });
    } else if (type == "SELF") {
      _.remove(food.detailSP.drugSelfList, function (drug) {
        return drug == order;
      });
    } else {
      console.log("type errer");
    }

    if (food.detailSP.drugCompanyList.length == 0 && food.detailSP.drugSelfList.length == 0) {
      delete food.detailSP;
    }
  }

  orderCallback = () => {
    console.log('callback');
    this.navCtrl.pop();
    this.isEdit = false;
    this.initFoodCart();
  }

  initFoodCart(){
    this.foodCart = {
      orderNo: null,
      foods: [],
      location: null,
      date: null,
      time: null,
      note: null,
    };
  }

  confirm() {
    console.log(this.foodCart);
    
    let isDataCorrect = true;
    if(!this.foodCart.location || !this.foodCart.date || !this.foodCart.time || this.foodCart.foods.length == 0){
      isDataCorrect = false;
      this.showErrorMsg();
      return;
    }

    if(this.foodCart.foods.length > 0){
      this.foodCart.foods.forEach(food => {
        if(!food.code || !food.name || !food.count || !food.spec || !food.unit || !this.checkNumber(food.count)){
          isDataCorrect = false;
          this.showErrorMsg();
          return;
        } else {
          //特配單包裝規格帶入飼料生產規格
          if (food.detailSP != null) {
            switch(food.spec) {
              case "PL": {
                food.detailSP.productSpec = "1";
                break;
              }
              case "ML": {
                food.detailSP.productSpec = "2";
                break;
              }
              case "CR": {
                food.detailSP.productSpec = "0";
                break;
              }
              default: {
                isDataCorrect = false;
                this.showErrorMsg();
                return;
              }
            }
          }
        }
      });
    }

    if(isDataCorrect){
      this.navCtrl.push(OrderConfirmPage, { order: this.foodCart, callback: this.orderCallback, animate: false });
    }
  }

  showErrorMsg(){
    this.alertProvider.create({
      message: "資料異常，請重新確認",
      buttons: [{
        text: "確定",
      }]
    });
  }

  cancel() {
    console.log('cancel');
    this.initFoodCart();
    this.weekDay = "";

    if(this.isEdit){
      this.isEdit = false;
      this.navCtrl.parent.select(1);
    }
  }

  checkNumber(value) {  
    var re = /^-?(\d*\.)?\d+$/; //正負小數點
    console.log("Check Number = " + re.test(value));
    return re.test(value);
  }

  back(){
    this.userProvider.reSelectSbuCustomer(this.appCtrl);
  }
}