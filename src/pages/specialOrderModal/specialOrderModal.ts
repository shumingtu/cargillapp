import { Component, Renderer } from '@angular/core';
import { NavController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { SpecialFoodModalPage } from '../specialFoodModal/specialFoodModal';
import * as _ from "lodash";
@Component({
  selector: 'page-special-order-modal',
  templateUrl: 'specialOrderModal.html',
})
export class SpecialOrderModalPage {
  food;
  drugOptions;
  drug;
  //specialOrderQuantitys;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    private modalCtrl: ModalController,
    private viewCtrl: ViewController) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'specialOrderModal', true);
  }
  ionViewWillLoad() {
    this.drugOptions = this.navParams.get('drugOptions');
    console.log(this.drugOptions);
    //this.specialOrdersQuantitys = this.navParams.get('specialFoodQuantitys');
    this.drug = {
      code: null,
      name: null,
      maxQuantity: null,
      recommendQuantity: null,
      count1kg: null,
      type: null,
    };
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialOrderModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.drug);
  }
  openSpecialFoodModal() {
    console.log("open modal");
    const drugModal = this.modalCtrl.create(SpecialFoodModalPage, {
      drugOptions: this.drugOptions,
      drug: this.drug,
      title: '選擇特配單'
    });
    drugModal.present();
    drugModal.onDidDismiss((chooseFoodId) => {
      console.log("choose food");
      console.log(chooseFoodId);
      if (chooseFoodId && chooseFoodId != this.drug.code) {
        this.drug.code = chooseFoodId;
        let chooseFood = _.find(this.drugOptions, function (option) {
          return option.drugId == chooseFoodId;
        });
        console.log(chooseFood);
        this.drug.name = chooseFood.drugName;
        this.drug.maxQuantity = chooseFood.max_amount;
        this.drug.recommendQuantity = chooseFood.recommended_amount;
        this.drug.count1kg = null;
        this.drug.type = chooseFood.type;
      }
    })
  }

}
