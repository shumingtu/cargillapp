import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { UserProvider } from '../../provider/userProvider';


import { FilterPipe } from '../../pipe/filterPipe'

import * as _ from "lodash";
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
  accounts;
  loginAccountId;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider) {
  }
  ionViewWillEnter() {
    this.accounts = this.navParams.get('accounts');
    if(!this.accounts){
      this.accounts = this.userProvider.getSubCustomers();
    }

    console.log(this.accounts);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  confirm() {
    let account = _.find(this.accounts, (account) => {
      return account.id == this.loginAccountId;
    });
    if (!account) return;
    localStorage.setItem('account', JSON.stringify(account));
    this.navCtrl.push(TabsPage, { animate: false });
  }
}
