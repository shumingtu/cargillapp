import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, ValidatorFn } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { Api } from '../../services/api';
import { AlertProvider } from '../../provider/alertProvider';
import { Constant } from '../../app/constant';

import { OtpPage } from '../otp/otp';

@Component({
  selector: 'page-activate',
  templateUrl: 'activate.html',
})
export class ActivatePage {

  errorMsg = "";
  activateData = { userId: "", password: "", confirmPassword: ""};
  phone = Constant.CS_PHONE;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public editForm: FormGroup = new FormGroup({
    'userId': new FormControl('', Validators.required),
    'password': new FormControl('', Validators.required),
    'confirmPassword': new FormControl('', [Validators.required]),
  });

  goOtp() {
    if ( this.activateData.password != this.activateData.confirmPassword ){
      this.errorMsg = "確認密碼不一樣";
      return;
    }
    this.errorMsg = "";

    console.log(this.activateData);
    this.navCtrl.push(OtpPage, { activateData: {userId:this.activateData.userId,password:this.activateData.password}}, { animate: false });
  }

  call() {
    let element: HTMLElement = document.getElementById('activatePhone') as HTMLElement;
    element.click();
  }
}
