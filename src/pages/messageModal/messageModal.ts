import { Component, Renderer } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-messageModal',
  templateUrl: 'messageModal.html',
})
export class MessageModalPage {
  message = {
    messageId: "",
    title: "",
    content: "",
    createTime: null,
  };

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public renderer: Renderer) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'messageModal', true);
  }

  ionViewWillLoad() {
    this.message = this.navParams.get('message');
    console.log(this.message);
    this.message.content = this.message.content.replace(/\n/g, "<br />");
    console.log(this.message);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodModalPage');
  }

  back() {
    this.viewCtrl.dismiss();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
