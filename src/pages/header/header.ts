import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-header',
  templateUrl: 'header.html',
})
export class HeaderPage {
  isHideBack = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(localStorage.reSelectSbuCustomer == 1){
      this.isHideBack = true;
      localStorage.removeItem("reSelectSbuCustomer");
    }
  }
  back() {
    this.navCtrl.pop({ animate: false });
  }

}
