import { Component, Renderer } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-specialFoodModal',
  templateUrl: 'specialFoodModal.html',
})
export class SpecialFoodModalPage {
  drugOptions;
  drugId;
  title;

  constructor(
    private navParams: NavParams,
    private viewCtrl: ViewController,
    public renderer: Renderer) {
      this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'foodModal', true);
  }
  ionViewWillLoad() {
    this.drugOptions = this.navParams.get('drugOptions');
    console.log(this.navParams.get('drug'));
    this.drugId = this.navParams.get('drug').code != 0 ? this.navParams.get('drug').code : this.drugOptions[0].drugId;
    this.title = this.navParams.get('title');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FoodModalPage');
  }
  back() {
    this.viewCtrl.dismiss();
  }
  closeModal() {
    this.viewCtrl.dismiss(this.drugId);
  }
}
