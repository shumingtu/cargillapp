import { Component } from '@angular/core';
import { App, NavController, NavParams } from 'ionic-angular';

import * as _ from "lodash";

import { Api } from '../../services/api';
import { AlertProvider } from '../../provider/alertProvider';
import { DBManager } from '../../services/dbManager';
import { CartManager } from '../../services/cartManager';
import { UserProvider } from '../../provider/userProvider';

import { OrderListPage } from '../orderList/orderList';

@Component({
  selector: 'page-order-confirm',
  templateUrl: 'orderConfirm.html',
})
export class OrderConfirmPage {

  account = {
    id: null,
    name: null,
    mcId: null,
    mcName: null,
    tmcode: null,
    phone: null,
  };

  appUser = {
    userId: null,
    name: null,
    type: null
  };

  order: any = {};
  callback;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertProvider: AlertProvider,
    public api: Api,
    public dbManager: DBManager,
    public cartManager: CartManager,
    private userProvider: UserProvider
  ) {
  }

  ionViewWillEnter() {
    //this.order = this.cartManager.list();
    this.account = JSON.parse(localStorage.getItem('account'));
    this.appUser = JSON.parse(localStorage.getItem('appUser'));
    this.order = this.navParams.get('order');
    this.callback = this.navParams.get('callback');

    this.order.customerId = this.account.id;
    this.order.customerName = this.account.name;
    this.order.orderPerson = this.appUser.name + "(" + this.appUser.userId + ")";
    this.order.tmcode = this.account.tmcode;
    this.order.customerPhone = this.account.phone;
    console.log(this.account);
    console.log(this.appUser);
    console.log(this.order);

  }

  ionViewWillLeave() {
    //this.navCtrl.popToRoot();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderConfirmPage');
  }

  addConfirm() {
    if (!localStorage.noRemind) {
      this.alertProvider.create({
        title: "貼心提醒",
        message: "親愛的客戶您好!" +
          "感謝您使用本系統下單訂料；訂單確認後系統將發送訂單完成推播。" +
          "出貨逾每日下午6點，將由客服人員與您確認到貨時間。",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: "不再提醒",
            handler: () => {
              this.alertProvider.clear();
              localStorage.noRemind = true;
              this.addOrder();
            }
          },
          {
            text: "確認",
            handler: () => {
              this.alertProvider.clear();
              this.addOrder();
            }
          }
        ]
      });
    } else {
      this.addOrder();
    }
  }

  addOrder() {
    console.log(this.order);

    if (!this.checkOrder()) {
      return;
    }
    console.log("送出訂單");
    this.cartManager.set("isOrdering", true);
    if (this.order.orderNo) {
      this.api.editOrder(this.order, (data) => {
        console.log(data);
        this.alertProvider.create({
          message: "修改訂單成功",
          enableBackdropDismiss: false,
          buttons: [{
            text: "確定",
            handler: () => {
              this.cartManager.remove("isOrdering");
              this.navCtrl.parent.select(1);
              this.callback();

              this.userProvider.getMessage().then((result) => { });
            }
          }]
        });
      }, (error) => {
        this.alertProvider.create({
          message: "修改訂單失敗<br/>" + error,
          buttons: [{
            text: "確定",
          }]
        })
        console.log(error);
      });
    } else {
      this.cartManager.set("isOrdering", true);
      this.api.addOrder(this.order, (data) => {
        console.log(data);
        this.alertProvider.create({
          message: "新增訂單成功",
          enableBackdropDismiss: false,
          buttons: [{
            text: "確定",
            handler: () => {
              this.cartManager.remove("isOrdering");
              this.navCtrl.parent.select(1);
              this.callback();

              this.userProvider.getMessage().then((result) => { });
            }
          }]
        });
        //this.navCtrl.parent.select(1);
        //this.callback();
      }, (error) => {
        this.alertProvider.create({
          message: "新增訂單失敗<br/>" + error,
          buttons: [{
            text: "確定",
          }]
        })
        console.log(error);
        this.cartManager.remove("isOrdering");
      });
    }
  }

  checkOrder() {
    console.log("check order");

    if (this.order.location == null) {
      this.alertProvider.create({
        message: "未填地點",
        buttons: [{
          text: "確定",
        }]
      })
      console.log("地點錯誤");
      return false;
    }

    if (this.order.date == null || this.order.time == null) {
      this.alertProvider.create({
        message: "未填時間",
        buttons: [{
          text: "確定",
        }]
      })
      console.log("時間錯誤");
      return false;
    }

    if (this.order.foods.length < 1) {
      this.alertProvider.create({
        message: "未填訂單內容",
        buttons: [{
          text: "確定",
        }]
      })
      console.log("沒有訂單內容");
      return false;
    }

    for (let i in this.order.foods) {
      if (this.order.foods[i].code == null) {
        this.alertProvider.create({
          message: "有未選的飼料項目",
          buttons: [{
            text: "確定",
          }]
        })
        console.log("未選擇飼料項目");
        return false;
      }

      //拿掉空的特配單
      if (this.order.foods[i].detailSP) {
        if (this.order.foods[i].detailSP.drugCompanyList.length == 0 && this.order.foods[i].detailSP.drugSelfList.length == 0) {
          delete this.order.foods[i].detailSP;
        }
      }
    }

    return true;
  }

  cancel() {
    this.navCtrl.pop();
  }
}
