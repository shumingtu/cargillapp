import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { PortalPage } from '../portal/portal';
import { AppVersion } from '@ionic-native/app-version';
import { Constant } from '../../app/constant';

@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  account;
  appUser;
  appVersion;

  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App,
    public nativeAppVersion: AppVersion) {
      if (typeof window['cordova'] !== 'undefined') {
        this.nativeAppVersion.getVersionNumber().then((deviceVersion) => {
            this.appVersion = deviceVersion + "." + Constant.APP_VERSION;
        });
    }else{
        this.appVersion = Constant.APP_VERSION;
    }
  }

  ionViewWillLoad() {
    this.account = JSON.parse(localStorage.getItem('account'));
    this.appUser = JSON.parse(localStorage.getItem('appUser'));
  }

  logout () {
    localStorage.removeItem('account');
    localStorage.removeItem('appUser');
    localStorage.removeItem('token');
    this.appCtrl.getRootNav().setRoot(PortalPage);
  }
}
